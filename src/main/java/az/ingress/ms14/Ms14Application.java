package az.ingress.ms14;

import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Ms14Application implements CommandLineRunner {



    public static void main(String[] args) {
        SpringApplication.run(Ms14Application.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

    }
}
