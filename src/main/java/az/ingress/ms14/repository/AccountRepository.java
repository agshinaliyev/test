package az.ingress.ms14.repository;

import az.ingress.ms14.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
