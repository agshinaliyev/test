package az.ingress.ms14.service;

import az.ingress.ms14.dto.AccountRequestDto;
import az.ingress.ms14.dto.AccountResponseDto;
import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository ;

    public Account getById(Long id) {
        return accountRepository.findById(id).orElseThrow(() -> new RuntimeException("Account not found"));
    }

    public AccountResponseDto create(AccountRequestDto dto) {
        Account account = Account.builder()
                .name(dto.getName())
                .balance(10000000.0)
                .build();
        account = accountRepository.save(account);
        return AccountResponseDto.builder()
                .id(account.getId())
                .name(account.getName())
                .balance(account.getBalance())
                .build();
    }
}
