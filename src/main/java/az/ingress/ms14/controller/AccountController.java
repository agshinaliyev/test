package az.ingress.ms14.controller;


import az.ingress.ms14.dto.AccountRequestDto;
import az.ingress.ms14.dto.AccountResponseDto;
import az.ingress.ms14.model.Account;
import az.ingress.ms14.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.json.HTTP;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;
    @GetMapping("/{id}")
    public ResponseEntity<Account> getById(@PathVariable Long id){
        Account account = accountService.getById(id);

        return ResponseEntity.status(HttpStatus.OK)
                .header("hello","12345")
                .body(account);
    }

    @PostMapping
    public ResponseEntity<AccountResponseDto> getById(@RequestBody AccountRequestDto accountRequestDto){
        AccountResponseDto accountResponseDto = accountService.create(accountRequestDto);

        return ResponseEntity.status(HttpStatus.CREATED)
                .header("hello","12345")
                .body(accountResponseDto);
    }
}
