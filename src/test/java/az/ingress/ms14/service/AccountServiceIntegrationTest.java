package az.ingress.ms14.service;

import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class AccountServiceIntegrationTest {

    @Autowired
    AccountService accountService;
    @Autowired
    AccountRepository accountRepository;


    @Container
    public static MySQLContainer mysql = new MySQLContainer("mysql:latest")
            .withUsername("root")
            .withPassword("password")
            .withDatabaseName("msyql-test");


    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mysql::getJdbcUrl);
        registry.add("spring.datasource.username", mysql::getUsername);
        registry.add("spring.datasource.password", mysql::getPassword);
        registry.add("spring.datasource.driver-class-name", mysql::getDriverClassName);

    }

    @BeforeAll
    public static void setUp() {
        mysql.start();
    }

    @Test
    void testGetAccountById() {

        //Arrange
        Account account = accountRepository.save(Account.builder()
                .name("Kanan")
                .balance(45.7)
                .build());
        //Act
        Account byId = accountService.getById(account.getId());

        //Assert

        assertThat(byId).isEqualTo(account);


    }

    @AfterAll
    public static void tearDown() {
        mysql.stop();
    }
}