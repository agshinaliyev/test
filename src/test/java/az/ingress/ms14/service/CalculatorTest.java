package az.ingress.ms14.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CalculatorTest {

    @InjectMocks
    private Calculator calculator;

    @Test
    void givenTwoDigitsWhenSumThenSuccess() {
        //Arrange
        int a = 5;
        int b = 10;

        //Act
        int sum = calculator.sum(a, b);

        //Assert
        assertThat(sum).isEqualTo(a + b);
    }

    @Test
    void givenTwoDigitsWhenDivideThenSuccess() {
        //Arrange
        int a = 5;
        int b = 10;

        //Act
        int sum = calculator.divide(b, a);

        //Assert
        assertThat(sum).isEqualTo(2);
    }

//    @Test
    void givenTwoDigitsWhenDivideToZeroThenException() {
        //Arrange
        int a = 10;
        int b = 0;

        //Act & assert
        assertThatThrownBy(() -> calculator.divide(a, b))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Divide to zero is prohibited");

    }

}