package az.ingress.ms14.controller;

import az.ingress.ms14.dto.AccountRequestDto;
import az.ingress.ms14.dto.AccountResponseDto;
import az.ingress.ms14.model.Account;
import az.ingress.ms14.service.AccountService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.valueOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(AccountController.class)
@RunWith(SpringRunner.class)
class AccountControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    public AccountService accountService;


    @Test
    void givenValidIdWhenGetAccountThenSuccess() throws Exception {
        Account account = Account.builder()
                .id(3L)
                .name("Agshin")
                .balance(300D)
                .build();

        //Arrange
        when(accountService.getById(anyLong())).thenReturn(account);

        //Act&Assert
        mockMvc.perform(get("/account/3")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().exists("hello"))
                .andExpect(content().json(objAsJson(account)))
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.name").value("Agshin"))
                .andExpect(jsonPath("$.balance").value(300D));


    }

    @Test
    void givenValidAccountRequestDtoWhenCreateThenSuccess() throws Exception {

        AccountRequestDto accountRequestDto = AccountRequestDto.builder()
                .name("Fariz")
                .balance(55.5)
                .build();

        //Arrange

        when(accountService.create(any())).thenReturn(AccountResponseDto.builder()
                .id(3L)
                .name("Rufat")
                .balance(555D)
                .build());

        //Act&assert
        mockMvc.perform(post("/account")
                        .contentType(APPLICATION_JSON)
                        .content(objAsJson(accountRequestDto))
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(3L))
                .andExpect(jsonPath("$.name").value("Rufat"))
                .andExpect(jsonPath("$.balance").value(555D));


    }

    private String objAsJson(Object object) throws JsonProcessingException {

        return mapper.writeValueAsString(object);


    }

}